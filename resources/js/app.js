/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Quill = require('quill')

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})

import moment from 'moment'

import { Form, HasError, AlertError } from 'vform';

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueSweetalert2 from 'vue-sweetalert2'
Vue.use(VueSweetalert2)

import { VueEditor, Quill } from "vue2-editor";

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('vue-editor', VueEditor)

import Multiselect from 'vue-multiselect'

Vue.component('multiselect', Multiselect)

Vue.component('submenu', require('./components/Settings/submenu.vue').default )

let routes = [
    { path: '/home', component: require('./components/Dashboard/index.vue').default },
    { path: '/profile', component: require('./components/Profile/index.vue').default },
    { path: '/settings/app', component: require('./components/Settings/app.vue').default },
    { path: '/settings/menu', component: require('./components/Settings/menu.vue').default },
    { path: '/settings/landing', component: require('./components/Settings/landing.vue').default },
    { path: '/settings/add-content-one', component: require('./components/Settings/add-content-one.vue').default },
    { path: '/settings/edit-content', name:'edit-content', component: require('./components/Settings/edit-content.vue').default },
    { path: '/settings/add-content-two', component: require('./components/Settings/add-content-two.vue').default },
    { path: '/blog/create-post', component: require('./components/Blog/create-post.vue').default },
    { path: '/blog/posts', component: require('./components/Blog/post.vue').default },
    { path: '/blog/edit-post', name:'edit-post', component: require('./components/Blog/edit-post.vue').default },
    { path: '/create-page', component: require('./components/Page/create-page.vue').default },
    { path: '/pages', component: require('./components/Page/pages.vue').default },
    { path: '/edit-page', name:'edit-page', component: require('./components/Page/edit-page.vue').default },
    { path: '/sliders', component: require('./components/Sliders/index.vue').default },
    { path: '/add-slider', component: require('./components/Sliders/add-slider.vue').default },
    { path: '/edit-slider', name:'edit-slider', component: require('./components/Sliders/edit-slider.vue').default },
    { path: '/gallery', component: require('./components/Gallery/index.vue').default },
    { path: '/create-gallery', component: require('./components/Gallery/create-gallery.vue').default },
    { path: '/edit-gallery', name:'edit-gallery', component: require('./components/Gallery/edit-gallery.vue').default },
    { path: '/messages', component: require('./components/Message/index.vue').default },
    { path: '/show-message', name:'show-message', component: require('./components/Message/show-message.vue').default },


    

    { path: '*', component: require('./components/errors/NotFound.vue').default }
  ]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

Vue.filter('upText', function(text){
  if (text) {
    return text.charAt(0).toUpperCase() + text.slice(1)
  }
})

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
});
