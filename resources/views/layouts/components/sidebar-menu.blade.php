<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->

      <li class="nav-item">
        <router-link to="/home" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt blue"></i>
            <p>Beranda</p>
        </router-link>
      </li>


      <li class="nav-item has-treeview">
        <a href="#blog" class="nav-link">
          <i class="nav-icon fas fa-newspaper orange"></i>
          <p>
            Blog
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview ml-3">
          <li class="nav-item">
            <router-link to="/blog/create-post" class="nav-link">
              <i class="fas fa-edit nav-icon teal"></i>
              <p>Buat Postingan</p>
            </router-link>
          </li>
          <li class="nav-item">
            <router-link to="/blog/posts" class="nav-link">
              <i class="fas fa-file-alt nav-icon cyan"></i>
              <p>Postingan</p>
            </router-link>
          </li>
        </ul>
      </li>

      <li class="nav-item has-treeview">
        <a href="#pages" class="nav-link">
          <i class="nav-icon fas fa-pager indigo"></i>
          <p>
            Halaman
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview ml-3">
          <li class="nav-item">
            <router-link to="/create-page" class="nav-link">
              <i class="fas fa-edit nav-icon yellow"></i>
              <p>Buat Halaman</p>
            </router-link>
          </li>
          <li class="nav-item">
            <router-link to="/pages" class="nav-link">
              <i class="fas fa-pager nav-icon text-white"></i>
              <p>Halaman</p>
            </router-link>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <router-link to="/sliders" class="nav-link">
            <i class="nav-icon fas fa-laptop-code purple"></i>
            <p>Slider</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link to="/gallery" class="nav-link">
            <i class="nav-icon fas fa-images pink"></i>
            <p>Galery</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link to="/messages" class="nav-link">
            <i class="nav-icon fas fa-envelope"></i>
            <p>Pesan</p>
        </router-link>
      </li>

      <li class="nav-item has-treeview">
        <a href="#settings" class="nav-link">
          <i class="nav-icon fas fa-cog red"></i>
          <p>
            Pengaturan
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview ml-3">
          <li class="nav-item">
            <router-link to="/settings/app" class="nav-link">
              <i class="fas fa-th nav-icon yellow"></i>
              <p>Aplikasi</p>
            </router-link>
          </li>
          <li class="nav-item">
              <router-link to="/settings/landing" class="nav-link">
                <i class="fas fa-home nav-icon blue"></i>
                <p>Halaman Depan</p>
              </router-link>
            </li>
          <li class="nav-item">
            <router-link to="/settings/menu" class="nav-link">
              <i class="fas fa-th nav-icon orange"></i>
              <p>Menu</p>
            </router-link>
          </li>
          <li class="nav-item">
            <router-link to="/profile" class="nav-link">
              <i class="fas fa-user nav-icon teal"></i>
              <p>Akun</p>
            </router-link>
          </li>
        </ul>
      </li>
    </ul>
</nav>