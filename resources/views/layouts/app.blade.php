<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $app->name }} - {{ $title }}</title>
    <link rel="icon" href="{{ asset('/assets/images/app/'.$app->icon) }}" type="image/x-icon" />


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta http-equiv="content-language" content="en-us">
    <meta http-equiv="content-type" content="text/HTML" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="{{ $app->description }}">
    <meta property="og:description" content="{{ $app->description }}" />
    <meta name="keywords" content="{{ $app->description }}">
    <meta property="og:title" content="{{ $app->title }}" />
    <meta name="author" content="Crescent-Theme">
    <meta name="copyright" content="copyright 2019 Crescent Theme">

    <link href="{{ asset('assets/landing/css/bootstrap.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/landing/css/font-awesome.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/landing/css/custom-font.css') }}" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">

    <link href="{{ asset('assets/landing/plugins/owl-carousel/css/owl.carousel.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/landing/plugins/owl-carousel/css/owl.theme.css') }}" rel="stylesheet" />

    @yield('style')

    <link href="{{ asset('assets/landing/css/navigation.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/landing/css/preloader.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/landing/css/style.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/landing/css/responsive.css') }}" rel="stylesheet" />
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50" class="home">

    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <header id="header" class="header_1 header_2">
        <div class="toolbar d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <div class="thim-have-any-question">
                            Ada pertanyaan ?
                            <div class="mobile"><i class="fa fa-phone m-r-5"></i><a class="value" href="tel:{{ $contact->phone }}"> {{ $contact->phone }}</a></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="child_login pull-right">
                            {{-- <i class="fa fa-clock-o"></i> Mon - Fri : 9.00AM - 06.00PM --}}
                            {{-- <a href="javascript:void(0)" class="register m-l-10 js-modal-show">Register</a>
                            <a href="javascript:void(0)" class="login js-modal-show">Login</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="study_nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light bg-faded mb-0 ">
                <a class="navbar-brand" href="/"><img src="{{ asset('/assets/images/app/'.$app->logo) }}" alt="Logo" width="50" class="float-left mr-2"> <h1 class="text-secondary">{{ $app->name }}</h1></a>
                    <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav nav lavalamp ml-auto menu main_menu">
                            
                            @foreach ($menus as $menu)
                            <li class="nav-item single_nav"><a href="{{ $menu['link'] }}" @if ($menu['newtab']) target="_blank" @endif class="nav-link">{{$menu['name']}}</a>
                                @if (count($menu['submenu']) != 0 OR count($menu['subpage']) != 0)
                                <ul class="navbar-nav nav mx-auto">
                                    @foreach ($menu['submenu'] as $submenu)
                                    <li class="nav-item"><a href="{{ $submenu->link }}" @if ($submenu->newtab) target="_blank" @endif class="nav-link">{{$submenu->name}}</a></li>    
                                    @endforeach
                                    @foreach ($menu['subpage'] as $page)
                                        <li class="nav-item"><a href="/page/{{$page->id}}" class="nav-link">{{$page->shortname}}</a></li>    
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    @yield('content')

    <footer>
            <div class="container"> 
                <div class="row">
    
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="footer_title">
                            <h2>Tentang Kami</h2>
                            <p>{{$app->description}}</p>
                            {{-- <ul class="social_icon">
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-skype"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul> --}}
                        </div>
                    </div>
    
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="footer_title">
                            <h2>{{$app->column_one}}</h2>
                            <ul class="footer_link">
                                @foreach ($column_one as $item)
                                <li><a href="{{$item->link}}"><i class="fa fa-chevron-right"></i> {{$item->title}}</a></li>    
                                @endforeach
                            </ul>
                        </div>
                    </div>
    
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="footer_title">
                            <h2>{{$app->column_two}}</h2>
                            <ul class="footer_link">
                                @foreach ($column_two as $item)
                                <li><a href="{{$item->link}}"><i class="fa fa-chevron-right"></i> {{$item->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="footer_title">
                <h2>Kontak</h2>
                <p>{{$contact->address}}</p>
                <ul class="contact_us">
                <li>Telepon : <a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></li>
                <li>Email : <a href="mailto:{{$contact->email}}">{{$contact->email}}</a></li>
                </ul>
                </div>
                </div>
                </div>
            </div>
        </footer>
    
        <div class="copy_right">
        <p>
        Copyright <i class="fa fa-copyright"></i> 2019 - Ronggosukowati Studio.
        </p>
        </div>
    
    
        <a class="btn btn-lg  scrollup"><i class="fa fa-arrow-up"></i></a>
    
        <script src="{{ asset('/assets/landing/js/jquery.min.js') }}"></script>
    
        <script src="{{ asset('assets/landing/js/bootstrap.min.js') }}"></script>
    
        <script type="text/javascript" src="{{ asset('assets/landing/js/jquery.countdown.min.js') }}"></script>
    
        <script src="{{ asset('assets/landing/plugins/owl-carousel/js/owl.carousel.min.js') }}"></script>
    
        <script src="{{ asset('assets/landing/js/jquery.magnific-popup.min.js') }}"></script>
        
        @yield('script')

        <script src="{{ asset('assets/landing/js/mobilemenu.js') }}"></script>
        <script src="{{ asset('assets/landing/js/custom.js') }}"></script>
    </body>
    
    </html>

