@extends('layouts.app')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('/assets/landing/plugins/fancybox/css/jquery.fancybox.min.css')}}" />
@endsection

@section('script')
<script type="text/javascript" src="{{asset('/assets/landing/plugins/fancybox/js/jquery.fancybox.min.js')}}"></script>
@endsection

@section('content')

<hr class="mt-0">
<div class="page_tital text-center">
    <h2>Galery</h2>
    <hr class="tital_border m-l-0">
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    @foreach ($galleries as $gallery)
                    <div class="col-md-4 col-sm-6">
                        <a href="{{ asset('/assets/images/gallery/'.$gallery->image) }}" data-fancybox="group" data-caption="{{$gallery->title}}">
                            <div class="teachers_pro custom_hover_img ">
                                <img class="img-responsive" alt="courese" src="{{ asset('/assets/images/gallery/'.$gallery->image) }}" height="200" style="object-fit:cover">
                            </div>
                        </a>
                        <div class="event_title">
                            <h4>{{$gallery->title}}</h4>
                            <p>
                            {{$gallery->description}}
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection