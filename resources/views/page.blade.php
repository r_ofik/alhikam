@extends('layouts.app')

@section('content')
<hr class="mt-0">

<section>
    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-9">
            <div class="blog_text blog_list">
                <h3>{{$post->title}}</h3>
                <span class="blog_com mb-0">
                    <i class="fa fa-calendar"></i> {{$post->created_at->format('D, d-M-Y h:i A')}}
                </span>
                <hr>
                {!!$post->content!!}
            </div>
        </div>
    
        <div class="col-sm-12 col-md-3">
            
            <div class="coures_searchbox">
                <form class="form-group" method="GET" action="/blog/search">
                    <div class="input-group form-group">
                        <input name="q" class="search-query form-control" placeholder="Pencarian..." type="search" required>
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="submit">
                            <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        
            <div class="post_title">
                <h4> Postingan Populer </h4>
                <hr class="tital_border_tow ml-0">
                @foreach ($populars as $popular)
                <div class="row mb-2">
                    <div class="col-xs-4 col-sm-2 col-md-5 col-4">
                        <div class="custom_hover_img">
                        <img class="img-responsive" alt="post" src="{{asset('assets/images/post-cover/'.$popular->image)}}" height="60" style="object-fit:cover">
                        </div>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-7 col-8">
                    <h5 class="post_text"><a href="/blog/{{$popular->id}}-{{urlencode($popular->title)}}.html"> {{$popular->title}}</a></h5>
                    <small class="text-secondary"> 
                        <i class="fa fa-calendar"></i> {{$popular->created_at->format('d-M-Y')}}    
                    </small>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="post_title">
                <h4> Postingan Terbaru </h4>
                <hr class="tital_border_tow ml-0">
                @foreach ($recents as $recent)
                <div class="row mb-2">
                    <div class="col-xs-4 col-sm-2 col-md-5 col-4">
                        <div class="custom_hover_img">
                        <img class="img-responsive" alt="post" src="{{asset('assets/images/post-cover/'.$recent->image)}}" height="60" style="object-fit:cover">
                        </div>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-7 col-8">
                    <h5 class="post_text"><a href="/blog/{{$recent->id}}-{{urlencode($recent->title)}}.html"> {{$recent->title}}</a></h5>
                    <small class="text-secondary"> 
                        <i class="fa fa-calendar"></i> {{$recent->created_at->format('d-M-Y')}}    
                    </small>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>


@endsection