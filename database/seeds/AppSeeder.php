<?php

use Illuminate\Database\Seeder;
use App\App;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::create([
            'name' => 'App Name',
            'title' => 'App Title',
            'icon' => 'icon.svg',
            'logo' => 'logo.svg',
            'description' => 'Default description',
            'column_one' => 'Kolom Pertama',
            'column_two' => 'Kolom Kedua'
        ]);
    }
}
