<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
            ['name'=> 'Beranda', 'link'=> '/'],
            ['name'=> 'Pendidikan', 'link'=> '#'],
            ['name'=> 'Program', 'link'=> '#'],
            ['name'=> 'Gallery', 'link'=> '/gallery'],
            ['name'=> 'Blog', 'link'=> '/blog'],
            ['name'=> 'Tentang Kami', 'link'=> '#'],
            ['name'=> 'Kontak', 'link'=> '/contact'],
        ];

        foreach($menu as $value){
            Menu::create($value);
        }
    }
}
