<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingColumn extends Model
{
    protected $guarded = ['id'];
}
