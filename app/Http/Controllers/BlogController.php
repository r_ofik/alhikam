<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\App;
use App\Contact;
use App\Menu;
use App\Page;
use App\LandingColumn as Landing;
use App\Post;
use App\Submenu;
use App\Message;
use Redirect;

class BlogController extends Controller
{
    public function index(){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();
        $data['title'] = 'Blog';

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();

        $data['posts'] = Post::where('status', 1)->latest()->paginate(9);

        return view('blog', $data);
    }

    public function show($id){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();

        $post = Post::find($id);

        $data['title'] = $post->title;

        $data['post'] = $post;
        
        $post->update([
            'viewer' => $post->viewer+1
        ]);
        
        $post->save();

        $data['populars'] = Post::where('status', 1)->orderBy('viewer', 'DESC')->limit(5)->get();

        $data['recents'] = Post::where('status', 1)->latest()->limit(5)->get();

        return view('detail-blog', $data);
    }

    public function search(Request $request){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();
        $data['title'] = 'Blog';

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();

        $data['posts'] = Post::where('title', 'LIKE', "%".$request->q."%")->where('status', 1)->latest()->paginate(9);

        return view('blog', $data);
    }

    public function page($id){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();

        $page = Page::find($id);

        $data['title'] = $page->title;

        $data['post'] = $page;
        
        $page->update([
            'viewer' => $page->viewer+1
        ]);
        
        $page->save();

        $data['populars'] = Post::where('status', 1)->orderBy('viewer', 'DESC')->limit(5)->get();

        $data['recents'] = Post::where('status', 1)->latest()->limit(5)->get();

        return view('detail-blog', $data);
    }

    public function contact(){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();
        $data['title'] = 'Kontak';

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();

        return view('contact', $data);
    }

    public function message(Request $request){
        $store = Message::create($request->all());

        return ($store) ? Redirect::to('/contact')->with('message', 'Pesan telah dikirim!') : Redirect::to('/contact')->with('failed', 'Pesan gagal dikirim!');
    }
}
