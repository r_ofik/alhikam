<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\App;
use App\Contact;
use App\Menu;
use App\Page;
use App\Slider;
use App\LandingColumn as Landing;
use App\Post;
use App\Submenu;

class LandingController extends Controller
{
    public function index(){
        $data['app'] = App::first();
        $data['contact'] = Contact::first();
        $data['title'] = 'Beranda';
        $data['column_one'] = Landing::where('column', 1)->get();
        $data['column_two'] = Landing::where('column', 2)->get();
        $data['posts'] = Post::latest()->paginate(5);

        $datamenu = Menu::get();
        
        foreach ($datamenu as $menu) {
            $data['menus'][] = [
                'name'      => $menu->name,
                'link'      => $menu->link,
                'newtab'    => $menu->newtab,
                'submenu'   => Submenu::where('menu', $menu->id)->get(),
                'subpage'   => Page::where('menu', $menu->id)->get()
            ];
        }

        $data['sliders'] = Slider::all();

        return view('landing', $data);
    }
}
