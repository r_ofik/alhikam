<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

class GaleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        return Gallery::all();
    }

    public function show($id){
        return Gallery::find($id);
    }

    public function createGalery(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if($request->image){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/gallery/').$name);
            $request->merge(['image' => $name]);

        }

        $store = Gallery::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Galery telah disimpan!'] : ['status' => "false", 'message' => 'Galery gagal disimpan!'];
    }

    public function updateGalery($id, Request $request){
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        $galery = Gallery::find($id);

        $currentImage = $galery->image;

        if($request->image != $currentImage){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/gallery/').$name);
            $request->merge(['image' => $name]);

            $galeryImage = public_path('assets/images/gallery/').$currentImage;
            if(file_exists($galeryImage)){
                @unlink($galeryImage);
            }

        }

        $update = $galery->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Galery telah disimpan!'] : ['status' => "false", 'message' => 'Galery gagal disimpan!'];
    }

    public function destroy($id){
        $galery = Gallery::find($id);

        $galeryImage = public_path('assets/images/gallery/').$galery->image;
        if(file_exists($galeryImage)){
            @unlink($galeryImage);
        }

        $delete = $galery->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
