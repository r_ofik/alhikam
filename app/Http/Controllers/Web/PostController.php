<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        return Post::latest()->paginate(15);
    }

    public function addPost(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required'
        ]);

        if($request->image){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/post-cover/').$name);
            $request->merge(['image' => $name]);

        }

        $store = Post::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Postingan telah disimpan!'] : ['status' => "false", 'message' => 'Postingan gagal disimpan!'];
    }

    public function updatePost($id, Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required'
        ]);
        $post = Post::find($id);

        $currentCover = $post->image;

        if($request->image != $currentCover){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/post-cover/').$name);
            $request->merge(['image' => $name]);

            $postCover = public_path('assets/images/post-cover/').$currentCover;
            if(file_exists($postCover)){
                @unlink($postCover);
            }

        }

        $update = $post->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Postingan telah disimpan!'] : ['status' => "false", 'message' => 'Postingan gagal disimpan!'];
    }

    public function show($id){
        return Post::find($id);
    }

    public function searchPost(){
        $q = \Request::get('q');
        return Post::where('title', 'LIKE', "%$q%")->paginate(15);
    }

    public function destroy($id){
        $post = Post::find($id);

        $postImage = public_path('assets/images/post-cover/').$post->image;
        if(file_exists($postImage)){
            @unlink($postImage);
        }

        $delete = $post->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
