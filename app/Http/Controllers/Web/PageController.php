<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        return Page::latest()->paginate(15);
    }

    public function addPage(Request $request){

        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'shortname' => 'required'
        ]);

        if($request->menu !=0){
        $request->merge(['menu' => $request->menu['id']]);
        }

        $store = Page::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Halaman telah disimpan!'] : ['status' => "false", 'message' => 'Halaman gagal disimpan!'];
    }

    public function updatePage($id, Request $request){

        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'shortname' => 'required'
        ]);

        if($request->menu !=0){
        $request->merge(['menu' => $request->menu['id']]);
        }

        $post = Page::find($id);

        $update = $post->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Halaman telah disimpan!'] : ['status' => "false", 'message' => 'Halaman gagal disimpan!'];
    }

    public function show($id){
        return Page::find($id);
    }

    public function searchPage(){
        $q = \Request::get('q');
        return Page::where('title', 'LIKE', "%$q%")->paginate(15);
    }

    public function destroy($id){
        $page = Page::find($id);

        $delete = $page->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
