<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\App;
use App\Contact;

class AppController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function appInfo(){
         return App::first();
    }

    public function updateApp(Request $request){
        $app = App::find($request->id);

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'name' => 'required|string|max:191',
            'description' => 'required|string|max:191',
        ]);

        $currentIcon = $app->icon;
        $currentLogo = $app->logo;

        if($request->icon != $currentIcon){
            $name = time().'.' . explode('/', explode(':', substr($request->icon, 0, strpos($request->icon, ';')))[1])[1];

            \Image::make($request->icon)->save(public_path('assets/images/app/').$name);
            $request->merge(['icon' => $name]);

            $userIcon = public_path('assets/images/app/').$currentIcon;
            if(file_exists($userIcon)){
                @unlink($userIcon);
            }

        }

        if($request->logo != $currentLogo){
            $name = time().'.' . explode('/', explode(':', substr($request->logo, 0, strpos($request->logo, ';')))[1])[1];

            \Image::make($request->logo)->save(public_path('assets/images/app/').$name);
            $request->merge(['logo' => $name]);

            $userLogo = public_path('assets/images/app/').$currentLogo;
            if(file_exists($userLogo)){
                @unlink($userLogo);
            }

        }

        $update = $app->update([
            'title' => $request->title,
            'name'  => $request->name,
            'description'   => $request->description,
            'icon'  => $request->icon,
            'logo'  => $request->logo
        ]);

        return ($update) ? ['status' => "true", 'message' => 'Data telah diubah!'] : ['status' => "false", 'message' => 'Data gagal diubah!'];
    }

    public function appContact(){
        return Contact::first();
    }

    public function updateContact(Request $request){
        $contact = Contact::find($request->id);

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|',
            'phone' => 'required|numeric|min:3',
            'address' => 'required'
        ]);

        $update = $contact->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address
        ]);

        return ($update) ? ['status' => "true", 'message' => 'Data telah diubah!'] : ['status' => "false", 'message' => 'Data gagal diubah!'];        
    }
}
