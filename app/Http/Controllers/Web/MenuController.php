<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index(){
        return Menu::all();
    }

    public function show($id){
        return Menu::find($id);
    }

    public function createMenu(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'link' => 'required'
        ]);

        $store = Menu::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Menu telah disimpan!'] : ['status' => "false", 'message' => 'Menu gagal disimpan!'];
    
    }

    public function updateMenu($id, Request $request){

        $this->validate($request,[
            'name' => 'required',
            'link' => 'required'
        ]);

        $menu = Menu::find($id);

        $update = $menu->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Menu telah disimpan!'] : ['status' => "false", 'message' => 'Menu gagal disimpan!'];
    }

    public function destroy($id){
        $menu = Menu::find($id);

        $delete = $menu->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
