<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        return Slider::all();
    }

    public function show($id){
        return Slider::find($id);
    }

    public function createSlider(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if($request->image){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/slider/').$name);
            $request->merge(['image' => $name]);

        }

        $store = Slider::create($request->all());

        return ($store) ? ['status' => "true", 'message' => 'Slider telah disimpan!'] : ['status' => "false", 'message' => 'Slider gagal disimpan!'];
    }

    public function updateSlider($id, Request $request){
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        $slider = Slider::find($id);

        $currentImage = $slider->image;

        if($request->image != $currentImage){
            $name = time().'.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];

            \Image::make($request->image)->save(public_path('assets/images/slider/').$name);
            $request->merge(['image' => $name]);

            $sliderImage = public_path('assets/images/slider/').$currentImage;
            if(file_exists($sliderImage)){
                @unlink($sliderImage);
            }

        }

        $update = $slider->update($request->all());

        return ($update) ? ['status' => "true", 'message' => 'Slider telah disimpan!'] : ['status' => "false", 'message' => 'Slider gagal disimpan!'];
    }

    public function destroy($id){
        $slider = Slider::find($id);

        $sliderImage = public_path('assets/images/slider/').$slider->image;
        if(file_exists($sliderImage)){
            @unlink($sliderImage);
        }

        $delete = $slider->delete();
        return ($delete) ? ['status' => "true", 'message' => 'Data telah dihapus!'] : ['status' => "false", 'message' => 'Data gagal dihapus!'];
    }
}
